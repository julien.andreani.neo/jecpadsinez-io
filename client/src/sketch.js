let size;
let resetButton;
let colorButtons;
let c;
let canvas;

function setup() {
    size = 8;
    c = 0;
    canvas = createCanvas(800, 800);
    canvas.mouseWheel(changeSize);
    background(255);

    resetButton = myCreateButton("Reset");
    resetButton.elem.mousePressed(() => background(255));

    colorButtons = [
        myCreateButton("Blue", "#0000ff"),
        myCreateButton("Red", "#ff0000"),
        myCreateButton("Green", "#00ff00"),
        myCreateButton("Yellow", "#ffff00"),
        myCreateButton("Cyan", "#00ffff"),
        myCreateButton("Purple", "#ff00ff")
    ];

    colorButtons.forEach(button => {
        button.elem.mousePressed(() => c = button.value);
    });
}

function draw() {
    stroke(c);
    strokeWeight(size);
    if (mouseIsPressed) {
        if (mouseX >= 0 && mouseX < width && mouseY >= 0 && mouseY < height)
            line(mouseX, mouseY, pmouseX, pmouseY);
    }
}

function myCreateButton(label, value) {
    return {
        elem: createButton(label),
        value: value
    };
}

function changeSize(event) {
    if (event.deltaY > 0)
        size += 5;
    else
        size -= 5;
    if (size < 1)
        size = 1;
    if (size > 50)
        size = 50;
}
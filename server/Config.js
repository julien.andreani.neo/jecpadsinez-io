const fs = require("fs");

class Config {
    static loadFile(file) {
        if (!fs.existsSync(file))
            return undefined;
        const contents = fs.readFileSync(file);
        const parsed = JSON.parse(contents);
        return parsed
    }
}

module.exports = Config;
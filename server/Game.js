const Config = require("./Config");

class Game {
    constructor(_rounds, _filePath) {
        this.players = [];
        this.points = [];
        this.rounds = _rounds;
        this.words = undefined;

        this.loadWords(_filePath);
    }

    run() {
        while (true) {

        }
    }

    addPlayer(player) {
        player.id = this.players.length;
        this.players.push(player);
    }

    removePlayer(id) {
        this.players.splice(id, 1);
    }

    loadWords(path) {
        const parsed = Config.loadFile(path);
        if (!parsed)
            throw ("File error");
        this.words = parsed;
    }

    debug() {
        console.log("Players : ");
        this.players.forEach(player => console.log(player));
        console.log("\nWords : ");
        this.words.forEach(word => console.log(word));
    }
}

module.exports = Game;
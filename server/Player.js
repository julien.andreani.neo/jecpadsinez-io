class Player {
    constructor(_name = "", _icon = undefined) {
        this.id = -1;
        this.name = _name;
        this.icon = _icon;
    }
}

module.exports = Player;
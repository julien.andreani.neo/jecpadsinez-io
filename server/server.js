// const Game = require("./Game");
// const Player = require("./Player");

// const game1 = new Game(0, "./resources/list1.json");

// game1.addPlayer(new Player("neo"));
// game1.addPlayer(new Player("choco"));

// game1.debug();

const express = require("express");
const app = express();
const server = require('http').createServer(app);
const socketio = require("socket.io");
const io = socketio(server);
const port = process.env.PORT || 8080;

// Starting server
server.on("error", (err) => console.error("Server error : ", err));
server.listen(port, () => console.log(`Server started on port ${port}`));

// Routing
const clientPath = `${__dirname}/../client`;
app.use(express.static(clientPath));

// io
io.on("connection", (socket) => {
    console.log("New user");

    socket.on("disconnect", (socket) => {
        console.log("User left");
    });
});